@extends('layouts.app')

@section('content')
    <div class="label-edit-popup">
        <h3>Редактировать расстояние</h3>
        <form action="" class="form-inline">
            <label for="label-edit-popup-label">Расстояние</label>
            <input id="label-edit-popup-label" value="1" autofocus
                   class="form-control" type="number">
        </form>
        <br>
        <input type="button" value="Сохранить" id="label-save-btn"
               class="btn btn-primary">
        <input type="button" value="Отменить" id="label-cancel-btn"
               class="btn btn-primary">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12" id="graph-editor">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <br>
                <button class="btn btn-primary" id="evaluate">
                    Посчитать!
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h3>Примеры из веба для сверки ответов:</h3>
                <form method="POST">
                    {!! csrf_field() !!}
                    <p>
                        <button name="pr1" class="btn btn-default">Пример1</button>
                    </p>
                    <p>
                        <button name="pr2" class="btn btn-default">Пример2</button>
                        <a href="http://baza-referat.ru/%D0%A0%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B7%D0%B0%D0%B4%D0%B0%D1%87%D0%B8_%D0%BA%D0%BE%D0%BC%D0%BC%D0%B8%D0%B2%D0%BE%D1%8F%D0%B6%D0%B5%D1%80%D0%B0_%D0%BC%D0%B5%D1%82%D0%BE%D0%B4%D0%BE%D0%BC_%D0%B2%D0%B5%D1%82%D0%B2%D0%B5%D0%B9_%D0%B8_%D0%B3%D1%80%D0%B0%D0%BD%D0%B8%D1%86"
                           target="blank">Источник</a>
                    </p>
                    <p>
                        <button name="pr3" class="btn btn-default">Пример3</button>
                    </p>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h2>Таблица длин маршрутов</h2>

                <form method="POST" class="form-inline">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <label for="amount">Размерность</label>
                        <input type="number" class="form-control"
                               placeholder="Размерность" id="amount"
                               name="amount" min="3"
                               value="{{ $table->size() }}">
                        <button class="btn btn-default form-inline"
                                type="submit" name="change">
                            Изменить размер таблицы
                        </button>
                    </div>

                    <div class="mt10">
                        {!! $table !!}
                    </div>

                    <button class="btn btn-primary" type="submit">
                        Посчитать!
                    </button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h4>Легенда узла: <sub>&lt;Номер шага обработки&gt;</sub>(Строка:Колонка)<sub>&lt;Стоимость&gt;</sub>
                </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div id="chart_div"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h2>{!! \App\Lib\Node::$answer !!}</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <button class="btn btn-default" id="prev_step">Предыдущий шаг
                </button>
                <button class="btn btn-default" id="next_step">Следующий шаг
                </button>
                <span>Шаг <span id="current_step"></span> из <span
                        id="total_steps"></span></span>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <?php $messages->printt(); ?>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    function isNodesConnected(nodeIdA, nodeIdB) {
        var result = false

        window.edges.forEach(function (edge) {
            if (edge.from === nodeIdA && edge.to === nodeIdB
                || edge.from === nodeIdB && edge.to === nodeIdA) {
                result = true
            }
        })

        return result
    }

    function getRandomArbitrary(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }

    <?php if (!isset($_POST['table'])): ?>
    localStorage.setItem('nodes', JSON.stringify([]))
    localStorage.setItem('edges', JSON.stringify([]))
    <?php endif; ?>

    var n = localStorage.getItem('nodes')
    var e = localStorage.getItem('edges')

    if (n !== null) {
        n = JSON.parse(n)
    } else {
        n = []
    }

    if (e !== null) {
        e = JSON.parse(e)
    } else {
        e = []
    }

    var counter = n.length + 1 || 1;
    var options = {
        manipulation: {
            enabled: true,
            initiallyActive: true,
            addNode: function (nodeData, callback) {
                nodeData.label = counter++
                callback(nodeData)
            },
            addEdge: function (edgeData, callback) {
                if (edgeData.from === edgeData.to) {
                    alert('You cannot connect the node to itself!')
                    return
                }

                if (isNodesConnected(edgeData.from, edgeData.to)) {
                    alert('Nodes already connected')
                    return
                }

                edgeData.label = getRandomArbitrary(1, 10)

                callback(edgeData)
            },
            editEdge: function (edgeData, callback) {
                $('.label-edit-popup').show()

                $('#label-save-btn').click(function () {
                    edgeData.label = $('#label-edit-popup-label').val()

                    $('.label-edit-popup').hide()
                    $('#label-save-btn').off()
                    $('#label-cancel-btn').off()

                    callback(edgeData);
                })

                $('#label-cancel-btn').click(function () {
                    $('.label-edit-popup').hide()
                    $('#label-save-btn').off()
                    $('#label-cancel-btn').off()

                    callback()
                })
            },
        },
        nodes: {
            physics: false,
        },
    }

    window.nodes = new vis.DataSet(n)
    window.edges = new vis.DataSet(e)
    window.graph = new vis.Network(document.getElementById('graph-editor'), {
        nodes: window.nodes,
        edges: window.edges,
    }, options)
</script>

<script type="text/javascript">
    var currentPageId = 1;
    var pagesAmount = +$('.page:last-child').attr('data-id');
    var $currentStep = $('#current_step');

    $('#total_steps').text(pagesAmount);
    $currentStep.text(currentPageId);

    $('#next_step').click(function (e) {
        $('.page[data-id=' + currentPageId + ']').hide();

        ++currentPageId;
        if (currentPageId > pagesAmount) {
            currentPageId = 1
        }

        $('.page[data-id=' + currentPageId + ']').show();
        $currentStep.text(currentPageId);
    });

    $('#prev_step').click(function (e) {
        $('.page[data-id=' + currentPageId + ']').hide();

        --currentPageId;
        if (currentPageId === 0) {
            currentPageId = pagesAmount
        }

        $('.page[data-id=' + currentPageId + ']').show();
        $currentStep.text(currentPageId);
    });
</script>

<script type="text/javascript">
    var prevId = '';

    google.load('visualization', '1', {packages: ['orgchart', 'corechart']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        data.addColumn('string', 'ToolTip');

        data.addRows([
            <?= $googleRows ?>
        ]);
        var chart = new google.visualization.OrgChart(
            document.getElementById('chart_div')
        );
        google.visualization.events.addListener(chart, 'ready', myReadyHandler);
        chart.draw(data, {allowHtml: true});
    }

    function myReadyHandler() {
        $('tr.google-visualization-orgchart-noderow-medium > td')
            .click(function () {
                var id = $(this).find('val').text();
                currentPageId = id;
                $currentStep.text(id);

                $('.page').each(function () {
                    $(this).hide('fast');
                    if (prevId != id && $(this).data('id') == id) {
                        $(this).toggle('medium');
                    }
                });

                prevId = id;
            });
    }
</script>
<script>
    $('#evaluate').click(function () {
        var table = {}
        var index = 1
        var nodesAmount = window.nodes.length
        var nodesArray = window.nodes.get()
        var edgesArray = window.edges.get()

        if (nodesAmount < 3) {
          alert('Минимальное количество вершин - 3')
          return
        }

        function getWeightBetweenNodes(from, to) {
            for (var i = 0, length = edgesArray.length; i < length; ++i) {
                var edge = edgesArray[i]

                if (edge.from === from.id && edge.to === to.id
                    || edge.from === to.id && edge.to === from.id) {
                    return +edge.label
                }
            }

            return 'INF'
        }

        nodesArray.forEach(function (node) {
            table[index] = {}

            _.range(1, nodesAmount + 1).forEach(function (item) {
                if (item === index) {
                    return
                }

                table[index][item] = getWeightBetweenNodes(node, nodesArray[item - 1])
            })

            ++index
        })

        var data = {
            _token: Laravel.csrfToken,
            amount: index - 1,
        }

        for (var rowNumber in table) {
            for (var colNumber in table[rowNumber]) {
                data['table[' + rowNumber + '][' + colNumber + ']'] = table[rowNumber][colNumber]
            }
        }


        localStorage.setItem('nodes', JSON.stringify(window.nodes.get()))
        localStorage.setItem('edges', JSON.stringify(window.edges.get()))

        var $form = $('<form>', {
            action: '/',
            method: 'post'
        });
        $.each(data, function (key, val) {
            $('<input>').attr({
                type: 'hidden',
                name: key,
                value: val,
            }).appendTo($form);
        });
        $form.appendTo('body').submit();
    })
</script>
@endpush
