<?php

namespace App\Lib;

/**
 * Хранение ключа строки и ключа колонки матрицы
 */
class Coords
{
    /**
     * @var mixed
     */
    public $row;

    /**
     * @var mixed
     */
    public $column;

    /**
     * @param mixed $row
     * @param mixed $column
     */
    public function __construct($row, $column)
    {
        $this->row = $row;
        $this->column = $column;
    }

    public function __toString()
    {
        return "($this->row:$this->column)";
    }
}
