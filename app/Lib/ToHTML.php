<?php

namespace App\Lib;

interface ToHTML
{
    /**
     * Функция вывода сохранененого ранее сообщения в классе реализующего интерфейс
     */
    public function printt();
}
