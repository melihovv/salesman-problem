<?php

namespace App\Lib;

/**
 * Сборник всего лога решения.
 */
class Messages
{
    /**
     * Хранит список сообщений для вывода на страницу.
     * @var array
     */
    public $messages = [];

    /**
     * Новая часть решения.
     * @param int $id
     */
    public function open($id)
    {
        $this->messages[] = new Pages($id);
    }

    /**
     * Закрыть часть решения.
     */
    public function close()
    {
        $this->messages[] = new Pages;
    }

    /**
     * Добавить в лог таблицу и текст.
     * @param array $table
     * @param string $text
     */
    public function add($table, $text)
    {
        $this->messages[] = new Message($table, $text);
    }

    /**
     * Вывод всего лога.
     */
    public function printt()
    {
        $res = '';
        foreach ($this->messages as $mess) {
            $res .= $mess->printt();
        }
        return $res;
    }
}
