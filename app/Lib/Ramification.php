<?php

namespace App\Lib;

/**
 * Для хранения множества строк и колонок.
 */
class Ramification
{
    /**
     * @var Coords[]
     */
    public $coords = [];

    /**
     * Добавление новых координат в массив.
     * @param mixed $row
     * @param mixed $column
     */
    public function add($row, $column)
    {
        $this->coords[] = new Coords($row, $column);
    }

    /**
     * Вывод всех элементов массива.
     * @return string
     */
    public function __toString()
    {
        $str = '';

        foreach ($this->coords as $coord) {
            $str .= "({$coord->row}:{$coord->column})";
        }

        return $str;
    }
}
