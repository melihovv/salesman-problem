<?php

if (!function_exists('get_current_action')) {
    /**
     * @return string
     */
    function get_current_action()
    {
        list(, $action) = explode(
            '@',
            Route::getCurrentRoute()->getActionName()
        );

        return $action;
    }
}

/**
 * Перегрузка функции max для исключения бесконечностей
 * @param array $arr
 * @return mixed
 */
function mymax($arr)
{
    foreach ($arr as $key => $value) {
        if (is_infinite($value)) {
            unset($arr[$key]);
        }
    }

    return $arr ? max($arr) : 0;
}

/**
 * Перегрузка функции min для исключения бесконечностей
 * @param array $arr
 * @return mixed
 */
function mymin($arr)
{
    foreach ($arr as $key => $value) {
        if (is_infinite($value)) {
            unset($arr[$key]);
        } else {
            break;
        }
    }

    return $arr ? min($arr) : 0;
}
