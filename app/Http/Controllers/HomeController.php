<?php

namespace App\Http\Controllers;

use App\Lib\BranchAndBound;
use App\Lib\Messages;
use App\Lib\Node;
use App\Lib\RowsGoogleCharts;
use App\Lib\TableBranchAndBound;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $messages = new Messages;
        BranchAndBound::$messages = $messages;
        Node::$messages = $messages;

        $table = new TableBranchAndBound;
        $root = new Node($table->table);
        $googleRows = new RowsGoogleCharts($root);

        $m = $messages->printt();

        return response()->json([
            'answer' => \App\Lib\Node::$answer,
            'messages' => $m,
        ]);
        //return view(
            //'home.index',
            //compact('messages', 'table', 'root', 'googleRows')
        //);
    }
}
